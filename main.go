package main

import (
	"context"
	"fmt"
	"net"

	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/go-mcproxy/mcnet"
	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
	"gitlab.com/eyJhb/go-mcproxy/proxy"
	"gitlab.com/eyJhb/go-mcproxy/proxy/hooks"
)

const (
	bindIP   = "0.0.0.0"
	bindPort = 25565

	serverIP   = "127.0.0.1"
	serverPort = 25560
)

func main() {
	fmt.Println(run())
}

func run() error {
	// incoming connections
	l, err := net.Listen("tcp", fmt.Sprintf("%s:%d", bindIP, bindPort))
	if err != nil {
		return err
	}
	defer l.Close()

	// server
	tcpAddr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%d", serverIP, serverPort))
	if err != nil {
		return err
	}

	fmt.Printf("Listening on %s:%d\n", bindIP, bindPort)
	fmt.Printf("Proxying to %s:%d\n", serverIP, serverPort)

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error().Err(err).Msg("failed to accept connection")
			continue
		}

		srvConn, err := net.DialTCP("tcp", nil, tcpAddr)
		if err != nil {
			log.Error().Err(err).Msg("failed to initiate connection to server")
			continue
		}

		c := proxy.Client{
			ClientConn: mcnet.WrapConn(conn),

			ServerConn: mcnet.WrapConn(srvConn),
			ServerIP:   serverIP,
			ServerPort: serverPort,

			VersionHuman:    proxy.DefaultMinecraftHumanVersion,
			VersionProtocol: proxy.DefaultMinecraftProtocolVersion,
		}

		// setup hooks
		c.Hooks.Connect("connect", hooks.Placement{}, playerConnected)
		c.Hooks.Disconnect("disconnect", hooks.Placement{}, playerDisconnected)
		c.Hooks.Status("status", hooks.Placement{}, minecraftStatus)

		go func() {
			if err := c.Start(); err != nil {
				log.Error().Err(err).Msg("exited handling client")
			}
		}()
	}

	return nil
}

func playerConnected(ctx context.Context, username string, hs packet.Handshake, pk *packet.HandshakeMessagePayload) error {
	log.Info().Str("username", username).Msg("player connected")

	return nil
}

func playerDisconnected(ctx context.Context, username string) error {
	log.Info().Str("username", username).Msg("player disconnected")

	return nil
}

func minecraftStatus(ctx context.Context, hs packet.Handshake, pk *packet.HandshakeStatusPayload) error {
	pk.Description.Text = "Yayayayay"
	pk.Players.Max = 255
	pk.Players.Online = 222

	return nil
}
