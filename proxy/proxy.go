package proxy

import (
	"context"
	"errors"
	"io"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/go-mcproxy/mcnet"
	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
	"gitlab.com/eyJhb/go-mcproxy/proxy/hooks"
)

const (
	// minecraft
	DefaultMinecraftHumanVersion    = "1.17.1"
	DefaultMinecraftProtocolVersion = 756
	MinecraftDefaultPort            = 25565
)

type Client struct {
	// ClientConn holds the client connection
	ClientConn *mcnet.Conn

	// ServerConn holds a connection to our server
	// that we are proxying to
	ServerConn *mcnet.Conn
	ServerIP   string
	ServerPort int16

	// versions
	VersionHuman    string
	VersionProtocol int

	// Hooks
	Hooks hooks.Manager

	// simple lock that we can use
	sync.Mutex
}

type HandshakeStatus struct {
	JSONResponse packet.String
}

func New(conn *mcnet.Conn) (*Client, error) {
	c := &Client{
		ClientConn: conn,

		VersionHuman:    DefaultMinecraftHumanVersion,
		VersionProtocol: DefaultMinecraftProtocolVersion,
	}

	// no need to use a goroutine here, as we already do that
	// when we accept a connection
	return c, c.Start()
}

func (c *Client) Start() error {
	// always run this
	defer func() {
		// close connection to the client (if any)
		if c.ClientConn != nil {
			c.ClientConn.Close()
		}

		// close connection to server (if any)
		if c.ServerConn != nil {
			c.ServerConn.Close()
		}
	}()

	for {
		ctx, cancel := context.WithTimeout(context.Background(), 60*time.Hour)

		// read a packet
		pk, err := c.ClientConn.ReadPacket()
		if err != nil {
			cancel()
			log.Error().Err(err).Msg("Failed to get packet, got SOME error, closing connection")
			return err

			if err == io.EOF {
				// return will run our defer and clone the connection
				log.Error().Err(err).Msg("Failed to get packet, got EOF, closing connection")
				return errors.New("got EOF, closing connection")
			}

			log.Error().Err(err).Msg("some error occurred while reading the packet")
			continue
		}

		// if we fail to process any packet, just continue
		if err := c.processPacket(ctx, pk); err != nil {
			log.Error().Err(err).Msg("failed to process packet")
		}

		cancel()
	}

	return nil
}

func (c *Client) processPacket(ctx context.Context, pk packet.Packet) error {
	// log packet information
	log.Info().
		Int32("packet_id", pk.ID).
		Int("length", len(pk.Data)).
		Msg("processing packet")

	// sanity check for packetID 0 with 0 length
	if pk.ID == 0 && len(pk.Data) == 0 {
		log.Info().Msg("packet with id = 0 and length = 0, skipping")
		return nil
	}

	if pk.ID == (packet.Handshake{}).ID() {
		if err := c.HandlerHandshake(ctx, pk); err != nil {
			return err
		}
	} else if pk.ID == (packet.Ping{}).ID() {
		var ping packet.Ping
		if err := packet.Unmarshal(pk.Data, &ping); err != nil {
			return err
		}

		// send it back to the client
		if err := c.ClientConn.SendPacket(packet.Pong{ping.Payload}); err != nil {
			return err
		}
	}

	return nil
}
