package hooks

import (
	"context"
	"errors"
	"sync"

	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
)

type HookPlacement uint8
type HookType uint8

const (
	PlaceDefault HookPlacement = iota
	PlaceBefore
	PlaceAfter

	TypeNone HookType = iota
	TypeStatus
	TypeConnect
	TypeDisconnect
)

var (
	// ErrStopChain should be retuned by any webhook function,
	// that wishes to stop the execution of webhooks.
	ErrStopChain = errors.New("stopped hooks chain")
)

type StatusFn func(ctx context.Context, hs packet.Handshake, pk *packet.HandshakeStatusPayload) error
type ConnectFn func(ctx context.Context, username string, hs packet.Handshake, pk *packet.HandshakeMessagePayload) error
type DisconnectFn func(ctx context.Context, username string) error

type Placement struct {
	Function string
	Type     HookPlacement
}

type HookResponse struct {
	Message string
}

type Hook struct {
	Name  string
	Type  HookType
	Place Placement

	Fn interface{}
}

type Manager struct {
	Hooks []Hook

	mutex sync.Mutex
}

func (m *Manager) swap(i, j int) {
	m.Hooks[i], m.Hooks[j] = m.Hooks[j], m.Hooks[i]
}

func (m *Manager) less(i, j int) bool {
	hi, hj := m.Hooks[i], m.Hooks[j]

	if hi.Place.Type == PlaceDefault && hj.Place.Type == PlaceDefault {
		return false
	}

	// check if hi/hj refers to each other
	if hi.Place.Function == hj.Name {
		return hi.Place.Type != PlaceBefore
	} else if hj.Place.Function == hi.Name {
		return hj.Place.Type == PlaceBefore
	}

	return false
}

func (m *Manager) order() {
	hl := len(m.Hooks)
	for i := 0; i < hl-1; i++ {
		if m.less(i, hl-1) {
			m.swap(i, hl-1)
		}
	}
}

func (m *Manager) HooksStatus() []StatusFn {
	ifns := m.getHooks(TypeStatus)
	var fns []StatusFn
	for _, fn := range ifns {
		fns = append(fns, fn.(StatusFn))
	}

	return fns
}

func (m *Manager) HooksConnect() []ConnectFn {
	ifns := m.getHooks(TypeConnect)
	var fns []ConnectFn
	for _, fn := range ifns {
		fns = append(fns, fn.(ConnectFn))
	}

	return fns
}

func (m *Manager) HooksDisconnect() []DisconnectFn {
	ifns := m.getHooks(TypeDisconnect)
	var fns []DisconnectFn
	for _, fn := range ifns {
		fns = append(fns, fn.(DisconnectFn))
	}

	return fns
}

func (m *Manager) getHooks(hType HookType) []interface{} {
	var fns []interface{}
	for _, hook := range m.Hooks {
		if hook.Type == hType {
			fns = append(fns, hook.Fn)
		}
	}

	return fns
}

func (m *Manager) Status(name string, placement Placement, fn StatusFn) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	m.Hooks = append(m.Hooks, Hook{
		Name:  name,
		Type:  TypeStatus,
		Place: placement,
		Fn:    fn,
	})

	m.order()
}

func (m *Manager) Connect(name string, placement Placement, fn ConnectFn) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	m.Hooks = append(m.Hooks, Hook{
		Name:  name,
		Type:  TypeConnect,
		Place: placement,
		Fn:    fn,
	})

	m.order()
}

func (m *Manager) Disconnect(name string, placement Placement, fn DisconnectFn) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	m.Hooks = append(m.Hooks, Hook{
		Name:  name,
		Type:  TypeDisconnect,
		Place: placement,
		Fn:    fn,
	})

	m.order()
}
