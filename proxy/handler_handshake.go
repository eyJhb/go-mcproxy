package proxy

import (
	"context"
	"encoding/json"
	"errors"
	"io"

	"github.com/rs/zerolog/log"
	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
)

const (
	DefaultStatusMsg = "Go Minecraft Proxy"
)

var (
	ErrClientOrServerConnNil = errors.New("either the client connection or server connection is not set")
)

func (c *Client) HandlerHandshake(ctx context.Context, pk packet.Packet) error {
	// read the packet
	var hs packet.Handshake
	if err := packet.Unmarshal(pk.Data, &hs); err != nil {
		log.Error().Err(err).Msg("Failed to decode handshake")
		return err
	}
	log.Debug().Interface("handshake", hs).Msg("got handshake packet")

	// status state, return a reply
	if hs.State == 1 {
		if err := c.handlerHandshakeStatus(ctx, hs); err != nil {
			return err
		}
		return nil
	}

	// login state, get next packet as username
	if hs.State == 2 {
		if err := c.handlerHandshakeLogin(ctx, hs); err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) handlerHandshakeLogin(ctx context.Context, hs packet.Handshake) error {
	// get login packet
	pk, err := c.ClientConn.ReadPacket()
	if err != nil {
		log.Error().Err(err).Msg("failed to get handshakeLogin packet")
		return err
	}

	// decode login and get username
	var handshakeLogin packet.HandshakeLoginStart
	if err := packet.Unmarshal(pk.Data, &handshakeLogin); err != nil {
		log.Error().Err(err).Msg("Failed to decode handshake login")
		return err
	}

	// run connect hooks
	var msg packet.HandshakeMessagePayload
	for _, hookFn := range c.Hooks.HooksConnect() {
		hookFn(ctx, string(handshakeLogin.Name), hs, &msg)
	}
	if msg != (packet.HandshakeMessagePayload{}) {
		return c.sendHandshakeMessage(ctx, msg)
	}

	// run disconnect hooks in defer function
	defer func() {
		for _, hookFn := range c.Hooks.HooksDisconnect() {
			hookFn(ctx, string(handshakeLogin.Name))
		}
	}()

	// now we need to imitate a login request from the client, to the server
	// and then afterwards we will just proxy the requests
	if err := c.proxyClient(ctx, hs, handshakeLogin); err != nil {
		return err
	}

	return nil
}

func (c *Client) proxyClient(ctx context.Context, hs packet.Handshake, hsl packet.HandshakeLoginStart) error {
	if c.ServerConn == nil || c.ClientConn == nil {
		return ErrClientOrServerConnNil
	}

	log.Info().Str("username", string(hsl.Name)).Msg("proxying client")

	// modify the hostname and port to match up
	hs.Hostname = packet.String(c.ServerIP)
	hs.Port = packet.Short(c.ServerPort)

	// send the handshake to the server
	if err := c.ServerConn.SendPacket(hs); err != nil {
		return err
	}

	// send the handshake login to the server
	if err := c.ServerConn.SendPacket(hsl); err != nil {
		return err
	}

	// proxying requires that initiateServerConnection has been called
	// beforehand, to initialise a connection to the real server
	// ensure connections goes in both ways
	// this blocks until the connection stops
	go io.Copy(c.ClientConn, c.ServerConn)
	_, err := io.Copy(c.ServerConn, c.ClientConn)

	return err
}

func (c *Client) handlerHandshakeStatus(ctx context.Context, hs packet.Handshake) error {
	handshakeStatusPayload := packet.HandshakeStatusPayload{
		Version: packet.HandshakeStatusPayloadVersion{
			Name:     c.VersionHuman,
			Protocol: c.VersionProtocol,
		},
		Players: packet.HandshakeStatusPayloadPlayers{
			Max:    100,
			Online: 0,
		},
		Description: packet.HandshakeStatusPayloadDescription{
			Text: DefaultStatusMsg,
		},
	}

	// run status hooks
	for _, hookFn := range c.Hooks.HooksStatus() {
		hookFn(ctx, hs, &handshakeStatusPayload)
	}

	data, err := json.Marshal(handshakeStatusPayload)
	if err != nil {
		log.Error().Err(err).Msg("failed to marshal generateHandshakeStatus")
		return err
	}

	if err := c.ClientConn.SendPacket(packet.HandshakeStatus{packet.String(data)}); err != nil {
		log.Error().Err(err).Msg("Failed to send SendPacket HandshakeStatus")
		return err
	}

	return nil
}

// helpers!
func (c *Client) sendHandshakeMessage(ctx context.Context, handshakeMessage packet.HandshakeMessagePayload) error {
	data, err := json.Marshal(handshakeMessage)
	if err != nil {
		log.Error().Err(err).Msg("failed to encode message for handshake")
		return err
	}

	if err := c.ClientConn.SendPacket(packet.HandshakeMessage{packet.String(data)}); err != nil {
		log.Error().Err(err).Msg("failed to send handshake message")
		return err
	}

	return nil
}
