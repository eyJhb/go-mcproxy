# Minecraft proxy

This is a simple Minecraft proxy, written in Golang.
The main goal of the proxy, is to be used with Minecraft on demand, where a VPS will be spun up upon a client connection.

Features:

- Getting username on client connection
- Ability to send "kick" message upon connection
- Get username on client disconnect
- Change information in the Minecraft server browser

`main.go` serves as a simple example, of how to use the proxy.

Checkout the Minecraft on demand project, that utilises this proxy [here](https://gitlab.com/eyJhb/go-minecraft-on-demand)
