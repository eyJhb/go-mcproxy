package packet

import (
	"bytes"
	"errors"
	"io"
	"reflect"
)

var (
	ErrMissingDecode = errors.New("Failed to unmarshal one of the types, as it does not implement a decode function")
	ErrMissingEncode = errors.New("Failed to marshal one of the types, as it does not implement a encode function")
)

type Packet struct {
	ID   int32
	Data []byte
}

func Unmarshal(data []byte, v interface{}) error {
	// reflect pointer to our struct
	refPtr := reflect.ValueOf(v)

	// ensure that we have a pointer, anything else cannot be used
	if refPtr.Kind() != reflect.Ptr {
		return errors.New("Unmarshal did not get a pointer to struct")
	}

	// get our underlying value
	ref := refPtr.Elem()

	// bytes.Reader of our data
	br := bytes.NewReader(data)

	// loop over all fields
	for i := 0; i < ref.NumField(); i++ {
		// get our current field first
		field := ref.Field(i)
		valuePtr := field.Addr()

		// get our decode function
		fnDecode := valuePtr.MethodByName("Decode")

		// ensure that we have the Decode function
		if !fnDecode.IsValid() {
			return ErrMissingDecode
		}

		// call our function with bytes.Reader parameter
		if err := fnDecode.Call([]reflect.Value{reflect.ValueOf(br)})[0].Interface(); err != nil {
			return err.(error)
		}
	}

	return nil
}

// We assume that this is a flat structure
// (it cannot be anything else really!)
// func Marshal(v interface{}) ([]byte, error) {
func Marshal(packetId int32, v interface{}) ([]byte, error) {
	// reflect the interface
	ref := reflect.ValueOf(v)

	var bs []byte

	// add ouc packetId to our byteslice
	bs = append(bs, VarInt(packetId).Encode()...)

	// loop over all fields
	for i := 0; i < ref.NumField(); i++ {
		// get our current field first
		field := ref.Field(i)

		// we need to call our encoding function here
		fnEncode := field.MethodByName("Encode")

		// ensure that we have the Encode function
		if !fnEncode.IsValid() {
			return nil, ErrMissingEncode
		}

		// call the function with no parameters
		output := fnEncode.Call(nil)[0]

		// append to our byteslice
		bs = append(bs, output.Bytes()...)
	}

	// prepend our total length to it
	bs = append(VarInt(len(bs)).Encode(), bs...)

	return bs, nil
}

func RecvPacket(r DecodeReader) (*Packet, error) {
	// get the length of the packet
	var l VarInt
	if err := l.Decode(r); err != nil {
		return nil, err
	}

	// ensure length are bigger than one
	if l < 1 {
		return nil, errors.New("packet length too short (l < 1)")
	}

	// read the packet length, `io.ReadFull` will return
	// a error if it could not read len(d)
	d := make([]byte, int(l))
	if _, err := io.ReadFull(r, d); err != nil {
		return nil, err
	}

	// turn the data into a buffer
	buf := bytes.NewBuffer(d)

	// read packet id
	var packetID VarInt
	if err := packetID.Decode(buf); err != nil {
		return nil, err
	}

	return &Packet{
		ID:   int32(packetID),
		Data: buf.Bytes(),
	}, nil
}
