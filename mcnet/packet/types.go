package packet

import (
	"errors"
	"io"
)

type (
	// Signed 16-bit integer, two's complement
	Short int16

	// Variable-length data encoding a two's complement signed 32-bit integer; more info in their section
	VarInt int32

	// Signed 64-bit integer, two's complement
	Long int64

	// A sequence of Unicode scalar values
	String string

	//ByteArray is []byte with prefix VarInt as length
	ByteArray []byte
)

// we want to be able to read a single byte,
// and be able to read more `.Read(buf)`
type DecodeReader interface {
	io.ByteReader
	io.Reader
}

// helper function to read more bytes
func ReadNBytes(r DecodeReader, n int) ([]byte, error) {
	bs := make([]byte, n)

	var err error
	for i := 0; i < n; i++ {
		bs[i], err = r.ReadByte()
		if err != nil {
			return nil, err
		}

	}

	return bs, nil
}

func (v Long) Encode() []byte {
	n := uint64(v)
	return []byte{
		byte(n >> 56), byte(n >> 48), byte(n >> 40), byte(n >> 32),
		byte(n >> 24), byte(n >> 16), byte(n >> 8), byte(n),
	}
}

func (v *Long) Decode(r DecodeReader) error {
	bs, err := ReadNBytes(r, 8)
	if err != nil {
		return err
	}

	*v = Long(int64(bs[0])<<56 | int64(bs[1])<<48 | int64(bs[2])<<40 | int64(bs[3])<<32 |
		int64(bs[4])<<24 | int64(bs[5])<<16 | int64(bs[6])<<8 | int64(bs[7]))
	return nil
}

func (v ByteArray) Encode() []byte {
	return append(VarInt(len(v)).Encode(), v...)
}

func (v *ByteArray) Decode(r DecodeReader) error {
	// get the length
	var l VarInt
	if err := l.Decode(r); err != nil {
		return err
	}

	// make our value into the correct size and read it
	*v = make([]byte, l)
	_, err := r.Read(*v)
	return err
}

func (v Short) Encode() []byte {
	n := uint16(v)

	return []byte{
		byte(n >> 8),
		byte(n),
	}
}

func (v *Short) Decode(r DecodeReader) error {
	bs, err := ReadNBytes(r, 2)
	if err != nil {
		return err
	}

	*v = Short(uint16(bs[0])<<8 | uint16(bs[1]))
	return nil
}

func (v String) Encode() []byte {
	byteString := []byte(v)
	var p []byte

	p = append(p, VarInt(len(byteString)).Encode()...)
	p = append(p, byteString...)

	return p
}

func (v *String) Decode(r DecodeReader) error {
	// get the length of the string
	var l VarInt
	if err := l.Decode(r); err != nil {
		return err
	}

	// log.Debug().Int32("length", int32(l)).Msg("Decoding string")

	bs, err := ReadNBytes(r, int(l))
	if err != nil {
		return err
	}

	*v = String(bs)
	return nil
}

func (v VarInt) Encode() []byte {
	var vi []byte
	num := uint32(v)
	for {
		tmp := byte(num & 0b01111111)
		num >>= 7
		if num != 0 {
			tmp |= 0b10000000
		}

		// log.Debug().
		// 	Uint8("tmp", uint8(tmp)).
		// 	Uint8("v", uint8(v)).
		// 	Str("component", "WriteVarX").
		// 	Msg("WriteVarX")

		vi = append(vi, byte(tmp))
		if num == 0 {
			break
		}
	}

	return vi
}

func (v *VarInt) Decode(r DecodeReader) error {
	var result uint64
	for i := 0; ; i++ {
		b, err := r.ReadByte()
		if err != nil {
			return err
		}

		// actually parse the results
		value := uint64(b & 0b01111111)

		// move value 7*i times to the left
		// ie. so that the first value is first
		// the next value if afterwards, etc.
		// basically we just reverse this
		result |= (value << (7 * i))

		// if it is more than X bits, then error
		// if it has MSB of 0, then break
		if i > 5 {
			return errors.New("VarInt is too lang")
		} else if (b & 0b1000000) == 0 {
			break
		}
	}

	*v = VarInt(result)

	// log.Debug().
	// 	Str("component", "ReadVarX").
	// 	Str("hex", fmt.Sprintf("0x%x", result)).
	// 	Uint64("integer", result).
	// 	// Str("string", string(result)).
	// 	Msg("Result")

	return nil
}
