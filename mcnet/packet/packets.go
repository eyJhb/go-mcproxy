package packet

//Handshake - https://wiki.vg/Protocol#Handshaking
type Handshake struct {
	ProtocolVersion VarInt
	Hostname        String
	Port            Short
	State           VarInt
}

func (p Handshake) ID() int32 {
	return 0x00
}

// Ping - https://wiki.vg/Protocol#Ping
type Ping struct {
	Payload Long
}

func (p Ping) ID() int32 {
	return 0x01
}

// Pong - https://wiki.vg/Protocol#Pong
type Pong struct {
	Payload Long
}

func (p Pong) ID() int32 {
	return 0x01
}

// Status - https://wiki.vg/Protocol#Status
type HandshakeStatus struct {
	Payload String
}

func (p HandshakeStatus) ID() int32 {
	return 0x00
}

type HandshakeStatusPayloadVersion struct {
	Name     string `json:"name"`
	Protocol int    `json:"protocol"`
}

type HandshakeStatusPayloadPlayerSample struct {
	Name string `json:"name"`
	Id   string `json:"id"`
}

type HandshakeStatusPayloadPlayers struct {
	Max    int                                  `json:"max"`
	Online int                                  `json:"online"`
	Sample []HandshakeStatusPayloadPlayerSample `json:"sample"`
}

type HandshakeStatusPayloadDescription struct {
	Text string `json:"text"`
}

type HandshakeStatusPayload struct {
	Version     HandshakeStatusPayloadVersion     `json:"version"`
	Players     HandshakeStatusPayloadPlayers     `json:"players"`
	Description HandshakeStatusPayloadDescription `json:"description"`
	Favicon     string                            `json:"favicon,omitempty"`
}

// HandshakeLoginStart - https://wiki.vg/Protocol#Login_Start
type HandshakeLoginStart struct {
	Name String
}

func (p HandshakeLoginStart) ID() int32 {
	return 0x0
}

// HandshakeMessage - https://wiki.vg/Protocol#Disconnect_.28login.29
type HandshakeMessage struct {
	Payload String
}

func (p HandshakeMessage) ID() int32 {
	return 0x00
}

type HandshakeMessagePayload struct {
	Text string `json:"text"`
	Bold bool   `json:"bold,string"`
}
