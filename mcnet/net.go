package mcnet

import (
	"net"

	"gitlab.com/eyJhb/go-mcproxy/mcnet/packet"
)

// basic interface to ensure the packet
// implements the ID() function
type PacketID interface {
	ID() int32
}

// our simple connection to either server
// or a client
type Conn struct {
	Socket net.Conn
}

func Dial(addr string) (*Conn, error) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	return WrapConn(conn), nil
}

func WrapConn(conn net.Conn) *Conn {
	return &Conn{
		Socket: conn,
	}
}

func (c *Conn) Close() error {
	return c.Socket.Close()
}

func (c *Conn) Read(p []byte) (int, error) {
	return c.Socket.Read(p)
}

func (c *Conn) ReadByte() (byte, error) {
	p := make([]byte, 1)
	if _, err := c.Socket.Read(p); err != nil {
		return 0, err
	}
	return p[0], nil
}

func (c *Conn) Write(p []byte) (int, error) {
	return c.Socket.Write(p)
}

// read a packet from our client
func (c *Conn) ReadPacket() (packet.Packet, error) {
	p, err := packet.RecvPacket(c)
	if err != nil {
		return packet.Packet{}, err
	}

	return *p, err
}

// write a packet to our client
func (c *Conn) SendPacket(spacket PacketID) error {
	// marshal our data
	bs, err := packet.Marshal(spacket.ID(), spacket)
	if err != nil {
		return err
	}

	_, err = c.Write(bs)
	return err
}
